name := "codelab1"
version := "0.1"

lazy val module1 = project in file("module1")

lazy val app = (project in file(".")).dependsOn(module1)
