val l1 = List(1, 2, 3)
println(l1)

val l2 = Nil
println(l2)

val l3 = Nil.::(5).::(6).::(7)
println(l3)

val l4 = l1 :: l3
println(l4)

val l5 = l1 ::: l3
println(l5)

val l6 = 6 :: (7 :: (8 :: Nil))
println(l6)

val l11: Seq[Int] = List(1, 2, 3)
println(l11)

val a12: Seq[Int] = Array(1, 2, 3)
println(a12)

val v1 = Vector(1, 2, 3, 4, 5)
println(v1)

val s1 = Set(1, 3, 6, 4, 1, 3, 4, 5, 6, 2, 3, 4, 5)
println(s1)