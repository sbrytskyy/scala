val f1 = (x: Int, y: Int) => x * y
f1(2, 3)

val l1 = List(1, 2, 3, 4, 5, 6, 7, 8)
val l11 = l1.map((x: Int) => x * x)
val l12 = l1.map(_ * 2)

val l2 = (-10 to 10).toList
val l21 = l2.map(x => x * x)

val f2 = (x:Int) => {
  if (x != 0) 1.0/x
}

val l3 = (-2 to 2).toList
val l31 = l3.map(f2)

val pf1: PartialFunction[Int, Double] = {
  case x: Int if x != 0 => 1.0/x
}
val l32 = l3.collect(pf1)
