import scala.collection.mutable
import scala.collection.immutable

val m1 = mutable.Map(1 -> "s", 2 -> "t")
println(m1)

m1(1) = "r"
println(m1)


val m2 = immutable.Map(1 -> "s", 2 -> "t")
println(m2)

m1(2) = "r"
println(m2)


for((number, string) <- m2) {
  println(s"number $number - string $string")
}