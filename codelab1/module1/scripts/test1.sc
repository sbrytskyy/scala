val xMax: Int = 5
val yMax: Int = 5

var x: Int = 0

while (x < xMax) {
  var y: Int = 0

  while (y < yMax) {
    println(s"x = $x, y = $y, x * y = ${x * y}")
    y += 1
  }

  x += 1
}
