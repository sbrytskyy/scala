val x: Int = 1

println(x)


def sumAndDiff(x: Int, y: Int): (Int, Int) = {
  val sum = x + y
  val diff = x - y

  (sum, diff)
}

println(sumAndDiff(5, 3))


def listSize(l: List[Any]): Int = {
  l.size
}

val l = List[Int](1, 2, 3, 4, 5)

val s = listSize(l)

println(s)