package demo

class Sample {

  val id:Int = 1

  val hello:String = "Hello"

  def doSomething(name: String): Unit = {
    println(s"$hello, $name")
  }
}
