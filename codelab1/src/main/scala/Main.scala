object Main {
  import demo.Sample

  def main(args: Array[String]): Unit = {
    println("Hello, Scala World!")

    val s1 = new Sample
    s1.doSomething("John")
  }
}
